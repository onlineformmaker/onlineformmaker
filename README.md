## Easy online form maker

### Build web forms, online surveys, questionnaires, and polls, in addition to easy ecommerce

Searching for form making? We are doing vital role that delivering all kind of forms

#### Our features:

* New forms generation
* Form Optimization
* Branch logic
* Payment integration
* Third party integration
* Push notifications
* Multiple language support
* Conditional logic
* Validation rules
* Server rules
* Custom reports
* A/B Testing

### Use our easy form builder to customize and design your form.
Choose from more than 1000 templates—ready to be customized by our [form maker](http://www.formlogix.com/Form_Maker.aspx). We Create visualizations made up of your own graphs, charts, and key metrics using our [online form maker](https://formtitan.com)

Happy form making!